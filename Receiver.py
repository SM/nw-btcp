#!/usr/local/bin/python3
import socket
import threading
import queue


class Receiver(threading.Thread):

    def __init__(self, buffer, sock):
        """Create a new UDP Receiver.

        Args:
            buffer (queue.Queue): the receive buffer.
            sock (socket.Socket): the socket to use
        """

        super().__init__()
        #self.setDaemon(True)
        self.buffer = buffer
        self.sock = sock
        self.done = False

    def run(self):
        while True and not self.done:
            try:
                dataaddr = self.sock.recvfrom(1016)
                self.buffer.put_nowait(dataaddr)
            except queue.Full:
                pass
            except socket.timeout:
                pass
            except OSError:
                break


    def stop(self):
        self.done = True
