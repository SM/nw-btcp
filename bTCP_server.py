#!/usr/local/bin/python3
import socket, argparse
from struct import *
import Btcp
from Packet import Header, Packet, Payload
import queue
import Runners
import time


class bTCP_server(Btcp.Btcp):

    def __init__(self, source, window = 100, timeout = 10):
        super().__init__(source, window, timeout)
        self.reassembleQueue = queue.PriorityQueue()
        self.reassemblesyns = []
        self.data = []
        self.finished = False

    def begin(self):
        self.start()

        # Start Handshake
        while not self.connected:
            self.connected = self.listen()

    def end(self):
        self.finished = True
        pass

    def receive_file(self, file):
        print("Start receiving file")
        self.data = []

        while self.connected and not self.finished:
            packet, addr = self.receivebuffer.get()
            packet = Packet.from_bytes(packet)
            if packet.header.id == self.id and packet.check_checksum():
                if packet.header.fin == 1:
                    print("FIN packet received.")
                    self.connected = False
                elif packet.header.synnumber == self.acknumber:
                    # print("Received packet with a sequence number of " + str(packet.header.synnumber))
                    self.add_data(packet)
                    self.send_ack()
                else:
                    # OUT of order packet
                    # print("An out-of-order packet has been received: " + str(packet.header.synnumber))
                    self.add_data(packet)
                    self.send_ack()
        timeout = time.time() + 2*self.timeout
        while not self.finished and time.time() < timeout:
            self.finished = self.respond_termination(addr, packet)
        if not self.finished:
            print("Terminated because of timeout. No ACK packet received from client.")

        print("All data has been received.")
        print("Starting writing data to file")
        file.from_packets(file.path, self.data)

        self.sender.stop()
        self.receiver.stop()
        self.sender.join()
        self.receiver.join()
        self.sock.close()

    def add_data(self, packet):
        try:
            while True:
                if packet.header.synnumber == self.acknumber:
                    self.data.append(packet.payload.to_bytes()[:packet.header.datalength])
                    self.acknumber += 1
                    syn, packet = self.reassembleQueue.get_nowait()
                    self.reassemblesyns.remove(packet.header.synnumber)
                elif packet.header.synnumber > self.acknumber:
                    if packet.header.synnumber not in self.reassemblesyns:
                        self.reassembleQueue.put((packet.header.synnumber, packet))
                        self.reassemblesyns.append(packet.header.synnumber)
                    break
                else:
                    break
        except queue.Empty:
            return
        except TypeError:
            print("Error")
        return

    def send_ack(self):
        header = Header(self.id, self.synnumber, self.acknumber, 0b10000, self.window, 1000, 0)
        ackpacket = Packet(header, Payload(bytes(1000)))
        ackpacket.set_checksum()
        self.sendbuffer.put((ackpacket.to_bytes(), self.peer))
        # print("Sent ACK with an ack-number of " + str(ackpacket.header.acknumber))


if __name__ == "__main__":
    #Handle arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-w", "--window", help="Define bTCP window size", type=int, default=100)
    parser.add_argument("-t", "--timeout", help="Define bTCP timeout in milliseconds", type=int, default=1000)
    parser.add_argument("-o", "--output", help="Where to store file", default="output.png")
    args = parser.parse_args()

    serverrunner = Runners.ServerRunner(args.window, args.timeout, args.output)
    serverrunner.start()

    #Define a header format
    #header_format = "I"
    #server.start()



""" 
while True:
    data, addr = sock.recvfrom(1016)
    print(unpack(header_format,data))
"""
