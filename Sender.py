#!/usr/local/bin/python3
import threading
import queue
import socket


class Sender(threading.Thread):

    def __init__(self, buffer, sock):
        """Create a new UDP Sender.

        Args:
            buffer (queue.Queue): the receive buffer.
            sock (socket.Socket): the socket to use
        """

        super().__init__()
        #self.setDaemon(True)
        self.buffer = buffer
        self.sock = sock
        self.done = False

    def run(self):
        while True and not self.done:
            try:
                data, addr = self.buffer.get(True, 1)
                self.sock.sendto(data, addr)
            except queue.Empty:
                pass
            except OSError:
                break

    def stop(self):
        self.done = True

