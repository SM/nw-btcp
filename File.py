import filecmp

class File(object):

    def __init__(self, path):
        self.path = path # Path of the file to be sent.

    def to_packets(self):

        packets = []

        with open(self.path, "rb") as f:
            print("Starting reading from " + self.path)
            bytes = f.read(1000)
            while bytes:
                packets.append(bytes)
                bytes = f.read(1000)
        f.close()

        return packets

    @staticmethod
    def from_packets(outputfile, bytes):

        with open(outputfile, "wb") as f:
            for blob in bytes:
                f.write(blob)
        f.close()

    def check_files(self, file):
        return filecmp.cmp(self.path, file.path)
