#!/usr/local/bin/python3
import threading
import bTCP_client
import bTCP_server
import File


class ClientRunner(threading.Thread):

    def __init__(self, window, timeout, input):
        super().__init__()
        self.window = window
        self.timeout = timeout
        self.input = input
        self.client = bTCP_client.bTCP_client(("127.0.0.2", 9001), ("127.0.0.1", 9001), self.window, self.timeout / 1000)

    def run(self):
        # Open Handshake
        self.client.begin()
        # Start File Transfer
        file = File.File(self.input)
        self.client.set_file(file)

        # Transfer Data
        self.client.send_file()

        # Terminate Handshake


class ServerRunner(threading.Thread):

    def __init__(self, window, timeout, output):
        super().__init__()
        self.window = window
        self.timeout = timeout
        self.output = output
        self.server = bTCP_server.bTCP_server(("127.0.0.1", 9001), self.window, self.timeout / 1000)

    def run(self):
        self.server.begin()
        file = File.File(self.output)
        self.server.receive_file(file)
