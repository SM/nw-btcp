from struct import *
import binascii


class Packet:

    def __init__(self, header, payload):
        self.header = header
        self.payload = payload

    def to_bytes(self):
        return self.header.to_bytes() + self.payload.to_bytes()

    @classmethod
    def from_bytes(cls, packet):
        header = Header.from_bytes(packet)
        payload = Payload.from_bytes(packet)

        return cls(header, payload)

    def calculate_checksum(self):
        checksum = binascii.crc32(self.header.get_checksum_data() + self.payload.data)
        return checksum

    def set_checksum(self, checksum=None):
        if checksum is None:
            checksum = self.calculate_checksum()
        self.header.checksum = checksum

    def check_checksum(self):
        return self.header.checksum == self.calculate_checksum()


class Header:

    def __init__(self, id, synnumber, acknumber, flags, window, datalength, checksum):
        self.id = id
        self.synnumber = synnumber
        self.acknumber = acknumber
        self._flags = flags
        self.window = window
        self.datalength = datalength
        self.checksum = checksum

    def to_bytes(self):
        return pack("!I2H2BHI",
                    self.id,
                    self.synnumber,
                    self.acknumber,
                    self._flags,
                    self.window,
                    self.datalength,
                    self.checksum)

    @classmethod
    def from_bytes(cls, packet):
        if len(packet) < 16:
            raise ValueError("header is too short")
        return cls(*unpack_from("!I2H2BHI", packet))

    def get_checksum_data(self):
        return pack("!I2H2BH",
                    self.id,
                    self.synnumber,
                    self.acknumber,
                    self._flags,
                    self.window,
                    self.datalength)

    @property
    def flags(self):
        return self._flags

    @flags.setter
    def flags(self, value):
        if value >= (1 << 8):
            raise ValueError("value too big for flags")
        self._flags = value

    @property
    def syn(self):
        """Get SYN flag."""
        return (self._flags >> 1) & 0b1

    @syn.setter
    def syn(self, value):
        """Set SYN flag."""
        if value:
            self._flags |= (1 << 1)
        else:
            self._flags &= ~(1 << 1)

    @property
    def ack(self):
        """Get ACK flag."""
        return (self._flags >> 4) & 0b1

    @ack.setter
    def ack(self, value):
        """Set ACK flag."""
        if value:
            self._flags |= (1 << 4)
        else:
            self._flags &= ~(1 << 4)

    @property
    def fin(self):
        """Get FIN flag."""
        return self._flags & 0b1

    @fin.setter
    def fin(self, value):
        """Set FIN flag."""
        self._flags &= ~0b1
        self._flags |= value


class Payload:

    def __init__(self, data):
        self.data = data

    def to_bytes(self):
        return self.data

    @classmethod
    def from_bytes(cls, packet):
        return cls(packet[16:])

    def zeroes(self):
        self.data = bytes(1000)

    def fill(self):
        length = len(self.data)
        self.data = self.data + bytes(1000 - length)

